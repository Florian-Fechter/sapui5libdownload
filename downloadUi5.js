
const fs = require('fs');
const path = require("path")
const ProgressBar = require('progress')
const Downloader = require('sapui5-downloader-core')

async function downloadVersion(downloader, version) {
    let downloadProgressBar
    return zipFile = await downloader.downloadSAPUI5(version, (total, progress) => {
        if (downloadProgressBar instanceof ProgressBar) {
            downloadProgressBar.tick(progress)
        } else {
            downloadProgressBar = new ProgressBar('Downloading: [:bar] :rate/bps :percent :etas', {
                complete: '=',
                incomplete: ' ',
                width: 20,
                total,
            })
        }
    })
}
async function extractZip(downloader, zipFile) {
    let extractionProgressBar
    await downloader.extractSAPUI5(zipFile, (total, progress) => {
        if (extractionProgressBar instanceof ProgressBar) {
            extractionProgressBar.tick(progress)
        } else {
            extractionProgressBar = new ProgressBar('Extracting: [:bar] :percent :etas', {
                complete: '=',
                incomplete: ' ',
                width: 20,
                total,
            })
        }
    })
}
async function cleanupDir(sDirPath){
    fs.readdir(sDirPath, (err, files) => {
        if (err) throw err;

        for (const file of files) {
            fs.unlink(path.join(sDirPath, file), err => {
                if (err) throw err;
            });
        }
    });
}

const yargs = require('yargs');

yargs.scriptName("downloadUi5")
    .usage('$0 <cmd> [args]')
    .command('download', 'Installs a special version', (yargs) => {
        yargs.option("ui5", {
            type: String,
            demandOption: true,
            describe: "The Ui5 version?!"
        })
        yargs.option("to", {
            type: String,
            demandOption: true,
            describe: "The destination folder?!"
        })
    }, async function (argv) {
        console.log("test")
        const tmpDir = path.resolve(`${__dirname}/tmp`)
        const downloader = new Downloader('Runtime', tmpDir, argv.to);
        const zipFile = await downloadVersion(downloader, argv.ui5);
        console.log("Cleanup Temp Files.");
        await extractZip(downloader, zipFile);
        console.log("Finished Extraction.");
        console.log("Cleanup Temp Files.");
        await cleanupDir(tmpDir);
        console.log("FINISHED");
    })
    .alias("download", "d")
    .help()
    .argv