# UI5 Downloader
## Description:
This simple tool should make it easy to download the UI5 standard coding for analyzing purposes. There is still potential for improvement. If you want to change something you can create a for or ask for changes.

## Preparation:

### Linux:
For linux systems you need to enable the execution-flag of the downloadUi5.sh file:
`chmod +x downloadUi5.sh`

## Usage:
The point of entry is the downloadUi5 command.

### Get possibilities:
With `downloadUi5 --help` you get alls the commandos there are.

### Download a version:
With `downloadUi5 download` command you can download a specific version to a specific directory (use `downloadUi5 download --help` to get more information).

## Interesting facts:
- To create the cli I used the `yargs` and `progress` library.
- To download the ui5 version I used the `sapui5-downloader-core` library.